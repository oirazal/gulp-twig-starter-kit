var gulp = require('gulp'),
	runSequence = require('run-sequence'),
	del = require('del'),
	sass = require('gulp-sass'),
	data = require('gulp-data'),
	twig = require('gulp-twig'),
	changed = require('gulp-changed'),
	cssnao = require('gulp-cssnano'),
	uglify = require('gulp-uglify'),
	htmlBeautify = require('gulp-html-beautify'),
	imagemin = require('gulp-imagemin'),
	plumber = require('gulp-plumber'),
	convertEncoding  = require('gulp-convert-encoding'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch'),
	wait = require('gulp-wait'),
	minimist = require('minimist'),
	fs = require('fs'),
	babel = require('gulp-babel');
	
var browserSync = require('browser-sync').create();

var options = {outPath: './build/',inPath : './src',wpPath : '../assets'};

var supported = [
		'Android >= 2.3',
		'BlackBerry >= 7',
		'Chrome >= 9',
		'Firefox >= 2',
		'Explorer >= 8',
		'iOS >= 5',
		'Opera >= 11',
		'Safari >= 5',
		'ChromeAndroid >= 9',
		'FirefoxAndroid >= 4',
		'ExplorerMobile >= 9'
	];

var args = minimist(process.argv.slice(2));

gulp.task('twig', function () {
	return gulp.src(options.inPath + '/templates/*.twig')
	.pipe(plumber({
		handleError: function (err){
			console.log(err);
			this.emit('end');
		}
	}))
	.pipe(twig())
	.on('error', function (err){
      process.stderr.write(err.message + '\n');
      this.emit('end');
	})
	.pipe(htmlBeautify())
	.pipe(gulp.dest(options.outPath));
});

gulp.task('fonts', function(){
	return gulp.src(options.inPath + '/assets/fonts/**/*{ttf,otf,eot,woff,woff2,svg}')
	.pipe(changed(options.outPath + '/assets/fonts'))
	.pipe(gulp.dest(options.outPath + '/assets/fonts'));
});

gulp.task('medias', function(){
	return gulp.src(options.inPath + '/medias/**/*{jpg,jpeg,png,gif,svg,mp4,webm,ogg}')
	.pipe(changed(options.outPath + '/medias'))
	.pipe(gulp.dest(options.outPath + '/medias'));
});

gulp.task('img', function(){
	return gulp.src(options.inPath + '/assets/img/**/*{jpg,jpeg,png,gif,svg,ico,json}')
	.pipe(changed(options.outPath + '/assets/img'))
	.pipe(gulp.dest(options.outPath + '/assets/img'));
});

gulp.task('js', function(){
	gulp.src(options.inPath + '/assets/js/lib/*.js')
	.pipe(changed(options.outPath + '/assets/js/lib'))
	.pipe(gulp.dest(options.outPath + '/assets/js/lib'));

	gulp.src(options.inPath + '/assets/js/plugin/**/*.js')
	.pipe(changed(options.outPath + '/assets/js/plugin'))
	.pipe(gulp.dest(options.outPath + '/assets/js/plugin'));

	gulp.src(options.inPath + '/assets/js/*.js')
	.pipe(babel({presets: ['es2015']}))
	.pipe(plumber())
	.pipe(changed(options.outPath + '/assets/js'))
	.pipe(gulp.dest(options.outPath + '/assets/js'))
	.pipe(uglify())
	.pipe(rename({
		suffix: '.min',
	}))
	.pipe(gulp.dest(options.outPath + '/assets/js'));
});

gulp.task('sass', function(){
	return gulp.src(options.inPath + '/assets/sass/*.scss')
	.pipe(plumber())
	.pipe(wait(500))
	.pipe(sass().on('error', sass.logError))
	.pipe(convertEncoding({to:'utf-8'}))
	.pipe(cssnao({
		autoprefixer: {browsers: supported, add: true}
	}))
	.pipe(gulp.dest(options.outPath + '/assets/css'));
});

gulp.task('mediasmin', function(){
	return gulp.src(options.inPath + '/medias/**/*{jpg,jpeg,png}')
	.pipe(imagemin())
	.pipe(gulp.dest(options.outPath + '/medias'));
});

gulp.task('imgmin', function(){
	return gulp.src(options.inPath + '/assets/img/**/*{jpg,jpeg,png}')
	.pipe(imagemin())
	.pipe(gulp.dest(options.outPath + '/assets/img'));
});

gulp.task('opt', function(cb){
	return runSequence(
		['imgmin'],
		['mediasmin'], 
		cb
	);
});

gulp.task('clean', function(){
	return del(options.outPath);
});

gulp.task('wp', function(){
	return gulp.src(options.outPath + '/assets/**/*').pipe(gulp.dest(options.wpPath));
});

gulp.task('cleanWp', function(){
	return del('../assets', {force: true});
});

gulp.task('build', function(cb){
	return runSequence(
		['clean', 'cleanWp'],
		['twig', 'sass', 'js', 'img', 'medias', 'fonts'],
		['wp'],
		cb
	);
});

gulp.task('server', function(){
	return browserSync.init({
        server: {
            baseDir: options.outPath,
            directory: true
        },
		localOnly: true, 
		reloadOnRestart: true,
		port: 8080,
        open: true,
        notify: false
    });
});

gulp.task('watcher', function(){
	watch(options.inPath + '/templates/**/*.twig', function(){
		gulp.start('twig');
	});
	watch(options.inPath + '/assets/sass/**/*.scss', function(){
		gulp.start('sass');
	});
	watch(options.inPath + '/assets/js/**/*.js', function(){
		gulp.start('js');
	});
	watch(options.inPath + '/assets/img/**/*{jpg,jpeg,png,gif,svg,ico,json}', function(){
		gulp.start('img');
	});
	watch(options.inPath + '/medias/**/*{jpg,jpeg,png,gif,svg,mp4,webm,ogg}', function(){
		gulp.start('medias');
	});
	watch(options.inPath + '/assets/fonts/*{ttf,otf,eot,woff,woff2,svg}', function(){
		gulp.start('fonts');
	});
	watch(options.outPath + '/assets/**/*', function(){
		gulp.start('wp');
	});
	
	gulp.watch(options.outPath + '/**/*').on('change', function(){
		browserSync.reload();
	});
});

gulp.task('default', function(cb){
	runSequence(
		['build'], 
		['server'], 
		['watcher'], 
		cb
	);
});