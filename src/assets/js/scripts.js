(function($){
    var app;

    app = {
        init : function() {
            $.mobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
            this._phoneMobile();
            this._rgpd();
		},
        _phoneMobile: function() {
            $('a[href^="tel:"]').on('click',function(){
                if(!$.mobile){
                    return false;
                }
            });
        },
        _rgpd: function() {

            if(typeof(readmoreLink) == 'undefined'){
                var readmoreLink = '';
            }

            tarteaucitron.init({
                "privacyUrl": readmoreLink,
                "hashtag": "#tarteaucitron",
                "cookieName": "rgpd",
                "orientation": "bottom",
                "showAlertSmall": false,
                "cookieslist": true,
                "adblocker": false,
                "AcceptAllCta" : true,
                "highPrivacy": false,
                "handleBrowserDNTRequest": false,
                "removeCredit": true, 
                "moreInfoLink": true, 
                "useExternalCss": true,           
                "readmoreLink": readmoreLink
            });

            (tarteaucitron.job = tarteaucitron.job || []).push('vimeo');
            (tarteaucitron.job = tarteaucitron.job || []).push('youtube');

            if(typeof(analyticsCode) != 'undefined'){
                tarteaucitron.user.gajsUa = analyticsCode;
                (tarteaucitron.job = tarteaucitron.job || []).push('gajs');
            }
        }
    };

    $(document).ready(function() {
        app.init();
    });
	
	$(window).on("scroll", function (){
        //
	});

}(jQuery));